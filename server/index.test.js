const config = require('./config/config.js');
const mongoose = require('mongoose');

// end test
function close() {
  process.exit(0);
}

// Connection and test mongo database
mongoose.connect(config.DATABASE_URI);
const db = mongoose.connection;
db.on('error', () => {
  console.error.bind(console, 'connection error:');
  close();
});
db.once('open', () => {
  console.log('conectado ao mongo');
  close();
});

