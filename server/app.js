const express = require('express');
const path = require('path');

const exampleController = require('./src/controllers/exampleController');

const app = express();

app.start = (args) => {
  app.set('port', args.PORT);

  // Priority serve any static files.
  app.use(express.static(path.resolve(__dirname, '../client/build')));

  // ===== EXAMPLE CODE =====
  // Answer API requests.
  app.use('/api', exampleController);
  // ===== ============ =====

  // All remaining requests return the React app, so it can handle routing.
  app.get('*', (request, response) => {
    response.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
  });

  app.listen(args.PORT, () => {
    console.error(`Node cluster worker ${process.pid}: listening on port ${args.PORT}`);
  });
};

module.exports = app;

