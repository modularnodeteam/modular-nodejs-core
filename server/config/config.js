module.exports = {
  PORT: process.env.PORT || 5000,
  DATABASE_URI: process.env.DATABASE_URI || 'mongodb://localhost/modular-nodejs',
};

