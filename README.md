# Modular Node-js

A Node.js system base to quickly start development, versioning on BitBucket and deploying on Heroku.

This repository contains:
- ERP Aplication core;
- ERP Client manager;

## Requirements
- node
- nodemon
- mongo

## Step by Step

### 1 -  Repository on Bitbucket
- Create a repository
- Clone it on your machine
- Download the [release](https://bitbucket.org/modularnodeteam/modular-nodejs-core/wiki/Releases) of this project that you wish and copy into your
cloned repository
- Replace all "modular-nodejs" to your project name (no spaces)
- Push the code.

### 2 - App on Heroku
- Create a new app on Heroku
- [Install mLab MongoDB](https://elements.heroku.com/addons/mongolab)

### 3 - Configure Bitbucket
- Enable Pipelines on repository settings (stop the Pipeline #1 because it is not configured)
- Set the Pilines / Environment Variables on repository settings
  - HEROKU_API_KEY = Get it in [heroku/account](https://dashboard.heroku.com/account)
  - HEROKU_APP_NAME = Heroku app name
  - DATABASE_URI = mongodb://127.0.0.1/[project name]

### 4 - Deploy from Bitbucket to Heroku
- Every push on master will make a new deploy.
- Or every rerun in repository / Pipilines

## References
- [heroku-cra-node](https://github.com/mars/heroku-cra-node): development/deploy solution repository;
- [create-react-app](https://github.com/facebook/create-react-app): frontend's base repository;
- [deploy-to-heroku](https://confluence.atlassian.com/bitbucket/deploy-to-heroku-872013667.html): auto bitbucket deploy on heroku
- [bitbucket-pipelines](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html): production/homologation branch deploy;
- [npm-arg](https://www.npmjs.com/package/arg): toggle the database URI;
- [node-feature](https://github.com/danielrohers/node-feature): Daniel Hoers's node base project
- [bitbucket-mongo](https://confluence.atlassian.com/bitbucket/test-with-databases-in-bitbucket-pipelines-856697462.html): test MongoDB on Bitbucket

## MongoDb connection on Bitbucket

A simple connection test [bitbucket-mongo](https://confluence.atlassian.com/bitbucket/test-with-databases-in-bitbucket-pipelines-856697462.html)

**server/mongo.test.js**
```
const config = require('./config/config.js');
const mongoose = require('mongoose');

// end test
function close() {
  process.exit(0);
}

// Connection and test mongo database
mongoose.connect(config.DATABASE_URI);
const db = mongoose.connection;
db.on('error', () => {
  console.error.bind(console, 'connection error:');
  close();
});
db.once('open', () => {
  console.log('conectado ao mongo');
  close();
});
```

**package.json**: script
```
"test": "cd server && node mongo.test",
```

**bitbucket/pipelines/repository-variables**

DATABASE_URI: mongodb://127.0.0.1/[any collection name, indedepends]

update bitbucket pipeline like this:

**bitbucket/pipeline**
```
image: node:6.9.4
pipelines:
  default:
    - step:
        script:
          - npm install
          - npm test
        services:
          - mongo

definitions:
  services:
    mongo:
      image: mongo
```